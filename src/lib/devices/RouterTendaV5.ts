import { AxiosError } from "axios";
import Byte from "../utils/Byte";
import IPAddress from "../utils/IPAddress";
import MACAddress from "../utils/MACAddress";
import Device, { DeviceTrafficStats } from "./Device";

interface IDHCPHost {
    name: string;
    ip: string;
    mac: string;
    static: boolean;
    time: number;
}

export default class RouterTendaV5 extends Device {
    private readonly trafficStatsUrl = "/goform/updateIptAccount";
    private readonly dhcpUrl = "/lan_dhcp_clients.asp";

    constructor(host: IPAddress) {
        super(host);

    }

    getTrafficStats(): Promise<DeviceTrafficStats[]> {
        return new Promise((resolve, reject) => {
            this._getTrafficStats()
                .then((trafficStatsList) => {
                    this.getDhcpList()
                        .then(hostList => {
                            main_loop:
                            for (const trafficStats of trafficStatsList) {
                                for (const host of hostList) {
                                    if (trafficStats.ip.toString() === host.ip) {
                                        if (host.mac) {
                                            try {
                                                trafficStats.mac = new MACAddress(host.mac);
                                            } catch {
                                                // TODO: Resolve this
                                            }
                                        }
                                        trafficStats.name = host.name ? host.name : (host.mac ? host.mac : trafficStats.ip.toString());
                                        continue main_loop;
                                    }
                                }
                            }
                            resolve(trafficStatsList);
                        })
                        .catch(() => {
                            resolve(trafficStatsList);
                        });
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

    private _getTrafficStats(): Promise<DeviceTrafficStats[]> {
        return new Promise((resolve, reject) => {
            this.axios.get(this.trafficStatsUrl, {
                headers: {
                    "Content-Type": "text/plain;charset=UTF-8",
                    "Cookie": "admin:language=en",
                    "Pragma": "no-cache",
                },
            })
                .then(response => {
                    try {
                        resolve(this.csvToTrafficStats(response.data));
                    } catch (e) {
                        reject(e);
                    }
                })
                .catch((err: AxiosError) => {
                    if (err.response) {
                        reject(new Error(`[${err.response.status}] ${err.response.statusText}`));
                    } else {
                        reject(new Error("Unexpected error"));
                    }
                });
        });
    }

    private getDhcpList(): Promise<IDHCPHost[]> {
        return new Promise((resolve, reject) => {
            this.axios.get(this.dhcpUrl, {
                headers: {
                    Cookie: "admin:language=en",
                    Pragma: "no-cache",
                },
            })
                .then(response => {
                    try {
                        const result = response.data.match(/var dhcpList=new Array\(.*\);/);
                        if (result === null) {
                            reject(new Error("Not found"));
                            return;
                        }
                        let listAsString = result[0];
                        listAsString = listAsString.replace("var dhcpList=new Array(", "");
                        listAsString = listAsString.replace(");", "");
                        listAsString = listAsString.replace(/[']/g, "");
                        resolve(this.csvToDhcpHosts(listAsString));
                    } catch (e) {
                        reject(e);
                    }
                })
                .catch((err: AxiosError) => {
                    if (err.response) {
                        reject(new Error(`[${err.response.status}] ${err.response.statusText}`));
                    } else {
                        reject(new Error("Unexpected error"));
                    }
                });
        });
    }

    private csvToTrafficStats(csv: string): DeviceTrafficStats[] {
        const rows = csv.split("\n");
        const trafficStatsList: DeviceTrafficStats[] = [];
        rows.forEach(row => {
            const stats = row.split(";");
            if (stats.length > 1) {
                const trafficStats = new DeviceTrafficStats(
                    new IPAddress(stats[0]),
                    new Byte({ kibibytes: parseInt(stats[1], 10) }),
                    new Byte({ kibibytes: parseInt(stats[2], 10) }),
                    new Byte({ mebibytes: parseInt(stats[4], 10) }),
                    new Byte({ mebibytes: parseInt(stats[6], 10) }),
                );
                trafficStatsList.push(trafficStats);
            }
        });
        return trafficStatsList;
    }

    private csvToDhcpHosts(csv: string): IDHCPHost[] {
        const rows = csv.split(",");
        const hostList: IDHCPHost[] = [];
        rows.forEach(row => {
            const host = row.split(";");
            if (host.length > 1) {
                hostList.push({
                    name: host[0],
                    ip: host[1],
                    mac: host[2],
                    static: host[3] === "1",
                    time: parseInt(host[4], 10),
                });
            }
        });
        return hostList;
    }
}
