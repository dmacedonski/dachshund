import { FieldType, Precision, TimePrecision } from "influx";
import { JsonConvert, JsonConverter, JsonCustomConvert, JsonObject, JsonProperty } from "json2typescript";
import InvalidValue from "../exceptions/InvalidValue";

@JsonConverter
export class PrecisionConverter implements JsonCustomConvert<string> {
    serialize(data: string): string {
        return data;
    }

    deserialize(data: any): string {
        if (typeof data !== "string") {
            throw new InvalidValue("Must be a 'n', 'u', 'ms', 's', 'm' or 'h'", "precision");
        }
        switch (data) {
            case "n":
                return Precision.Nanoseconds;
            case "u":
                return Precision.Microseconds;
            case "ms":
                return Precision.Milliseconds;
            case "s":
                return Precision.Seconds;
            case "m":
                return Precision.Minutes;
            case "h":
                return Precision.Hours;
            default:
                throw InvalidValue.value(data, "Must be a 'n', 'u', 'ms', 's', 'm' or 'h'", "precision");
        }
    }
}

@JsonConverter
export class TypeConverter implements JsonCustomConvert<FieldType> {
    serialize(data: FieldType): string {
        return FieldType[data];
    }

    deserialize(data: any): FieldType {
        if (typeof data !== "string") {
            throw new InvalidValue("Must be a 'integer', 'float', 'string', 'boolean'", "type");
        }
        switch (data) {
            case "integer":
                return FieldType.INTEGER;
            case "float":
                return FieldType.FLOAT;
            case "string":
                return FieldType.STRING;
            case "boolean":
                return FieldType.BOOLEAN;
            default:
                throw InvalidValue.value(data, "Must be a 'integer', 'float', 'string', 'boolean'", "type");
        }
    }
}

@JsonObject
export class InfluxField {
    @JsonProperty("name", String)
    name = "";

    @JsonProperty("type", TypeConverter)
    type = FieldType.FLOAT;
}

@JsonObject
export class InfluxSchema {
    @JsonProperty("measurement", String, true)
    measurement = "";

    @JsonProperty("precision", PrecisionConverter, true)
    precision: TimePrecision = Precision.Milliseconds as TimePrecision;

    @JsonProperty("retention_policy", String, true)
    retentionPolicy: string = "";

    @JsonProperty("fields", [InfluxField], true)
    fields: InfluxField[] = [];

    @JsonProperty("tags", [String], true)
    tags: string[] = [];
}

@JsonObject
export default class InfluxConfigModel {
    @JsonProperty("host", String, true)
    host = "127.0.0.1";

    @JsonProperty("port", Number, true)
    port = 8086;

    @JsonProperty("user", String, true)
    user: string = "";

    @JsonProperty("password", String, true)
    password: string = "";

    @JsonProperty("database", String, true)
    database = "";

    @JsonProperty("schema", InfluxSchema, true)
    schema = new InfluxSchema();

    private converter = new JsonConvert();

    extend(json: object): InfluxConfigModel {
        const baseJson = this.converter.serialize(this);
        return this.converter.deserialize({ ...baseJson, ...json }, InfluxConfigModel);
    }
}
