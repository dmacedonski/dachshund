import { JsonObject, JsonProperty } from "json2typescript";
import InfluxConfigModel from "./InfluxConfigModel";
import TaskModel from "./TaskModel";

@JsonObject()
export default class ConfigModel {
    @JsonProperty("log_level", String, true)
    logLevel = "info";

    @JsonProperty("influx", InfluxConfigModel, true)
    influx = new InfluxConfigModel();

    @JsonProperty("tasks", [TaskModel])
    tasks: TaskModel[] = [];
}
