import { JsonConverter, JsonCustomConvert, JsonObject, JsonProperty } from "json2typescript";
import InvalidValue from "../exceptions/InvalidValue";
import { Duration } from "../utils/Duration";
import InputConfigModel from "./InputConfigModel";
import OutputConfigModel from "./OutputConfigModel";

@JsonConverter
export class DurationConverter implements JsonCustomConvert<Duration> {
    serialize(data: Duration): number {
        return data.inSeconds;
    }

    deserialize(data: any): Duration {
        if (typeof data !== "number") {
            throw new InvalidValue("Must be a number", "precision");
        }
        return new Duration({ seconds: data });
    }
}

@JsonObject
export default class TaskModel {
    @JsonProperty("name", String)
    name: string = null;

    @JsonProperty("interval", DurationConverter)
    interval: Duration = null;

    @JsonProperty("input", InputConfigModel)
    input: InputConfigModel = null;

    @JsonProperty("output", OutputConfigModel)
    output: OutputConfigModel = null;
}
