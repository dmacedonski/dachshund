import { expect } from "chai";
import { describe } from "mocha";
import InvalidValue from "../exceptions/InvalidValue";
import { Duration } from "../utils/Duration";
import { DurationConverter } from "./TaskModel";

describe("DurationConverter", () => {
    describe("#deserialize", () => {
        let converter: DurationConverter = null;

        before(() => {
            converter = new DurationConverter();
        });

        it("should return InvalidValue exception if data is not number", () => {
            const func = () => {
                return converter.deserialize("3");
            };

            expect(func).to.throw(InvalidValue);
        });

        it("should return Duration object with 3 seconds if data is number", () => {
            expect(converter.deserialize(3).inSeconds).to.equal(3);
        });

        it("should return InvalidValue exception if data is number < 0", () => {
            const func = () => {
                return converter.deserialize(-1);
            };

            expect(func).to.throw(InvalidValue);
        });
    });

    describe("#serialize", () => {
        let converter: DurationConverter = null;

        before(() => {
            converter = new DurationConverter();
        });

        it("should return 3 if data is Duration with 3 seconds", () => {
            expect(converter.serialize(new Duration({ seconds: 3 }))).to.equal(3);
        });
    });
});
