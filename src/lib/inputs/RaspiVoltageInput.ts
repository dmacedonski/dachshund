import { exec } from "child_process";
import { JsonConvert, JsonObject, JsonProperty } from "json2typescript";
import moment = require("moment");
import IInput, { InputData } from "./Input";

@JsonObject
class RaspiVoltageInputOptions {
    /**
     * Path to `vcgencmd`
     * Default: `/opt/vc/bin/vcgencmd`
     */
    @JsonProperty("vcgencmdPath", String, true)
    vcgencmdPath = "/opt/vc/bin/vcgencmd";
}

export default class RaspiVoltageInput implements IInput {
    options: RaspiVoltageInputOptions;

    constructor(options = {}) {
        const convert = new JsonConvert();
        this.options = convert.deserialize(options, RaspiVoltageInputOptions);
    }

    getData(): Promise<InputData> {
        return new Promise((resolve, reject) => {
            exec(`${this.options.vcgencmdPath} measure_volts`, (err, stdout) => {
                if (err) {
                    reject(err);
                    return;
                } else if (stdout.indexOf("volt") >= 0) {
                    let volts = stdout.replace("volt=", "");
                    volts = volts.replace("V", "");
                    resolve(new InputData(moment(), { value: parseFloat(volts) }));
                } else {
                    reject(new Error("Can not parse string"));
                }
            });
        });
    }
}
