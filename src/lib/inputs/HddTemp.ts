import {JsonConvert, JsonObject, JsonProperty} from "json2typescript";
import IInput, {InputData} from "./Input";
import * as moment from "moment";
import {Moment} from "moment";
import * as net from "net";
import * as winston from "winston";

@JsonObject
class HddTempOptions {
    /**
     * Service host.
     * Default: `localhost`
     */
    @JsonProperty("host", String, true)
    host = "localhost";
    /**
     * Service port.
     * Default: `7634`
     */
    @JsonProperty("port", Number, true)
    port = 7634;
    /**
     * Service delimiter.
     * Default: `|`
     */
    @JsonProperty("delimiter", String, true)
    delimiter = "|";
}

export default class HddTemp implements IInput {
    private logger: winston.Logger;
    private options: HddTempOptions;

    constructor(options = {}, logger: winston.Logger) {
        const convert = new JsonConvert();
        this.options = convert.deserialize(options, HddTempOptions);
        this.logger = logger;
    }

    getData(): Promise<InputData[]> {
        const timestamp: Moment = moment();
        return new Promise((resolve, reject) => {
            const socket = new net.Socket();
            socket.setEncoding("UTF8");
            socket
                .connect(this.options.port, this.options.host, () => {
                    this.logger.debug("Hddtemp socket connected");
                })
                .on("data", data => {
                    socket.destroy();
                    resolve(this.parseResponse(timestamp, data.toString()));
                })
                .on("error", err => {
                    reject(err.message);
                });
        });
    }

    private parseResponse(timestamp: Moment, data: string): InputData[] {
        const res: InputData[] = [];
        const delimiterLen = this.options.delimiter.length;
        const arr = data.substr(delimiterLen).slice(0, -delimiterLen).split(this.options.delimiter.repeat(2));
        arr.forEach((value: string) => {
            const params = value.split(this.options.delimiter);
            res.push(new InputData(timestamp, {temp: parseFloat(params[2])}, {device: params[0], name: params[1]}));
        });
        return res;
    }
}
