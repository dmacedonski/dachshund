import { exec } from "child_process";
import { JsonConvert, JsonObject, JsonProperty } from "json2typescript";
import { Moment } from "moment";
import moment = require("moment");
import IPAddress, { IPAddressConverter } from "../utils/IPAddress";
import IInput, { InputData } from "./Input";

@JsonObject
class Host {
    /**
     * Host name, eg. `Google DNS`
     */
    @JsonProperty("name", String)
    name: string = "";
    /**
     * Host address, eg. `8.8.8.8`
     */
    @JsonProperty("address", IPAddressConverter)
    address: IPAddress = new IPAddress("127.0.0.1");
}

@JsonObject
class PingInputOptions {
    /**
     * Hosts list
     */
    @JsonProperty("hosts", [Host])
    hosts: Host[] = [];
    /**
     * Count of pings per host.
     * Default: `4`
     */
    @JsonProperty("count", Number, true)
    count = 4;
    /**
     * Timeout for one ping, eg. if count is `5` and timeout `2s` total timeout is `10s`.
     * Default: `2`
     */
    @JsonProperty("timeout", Number, true)
    timeout = 2;
}

export default class PingInput implements IInput {
    protected options: PingInputOptions;

    constructor(options = {}) {
        const convert = new JsonConvert();
        this.options = convert.deserialize(options, PingInputOptions);
    }

    getData(): Promise<InputData[]> {
        const timestamp: Moment = moment();
        const pings: Array<Promise<InputData>> = [];

        this.options.hosts
            .forEach(host => {
                pings.push(new Promise<InputData>((resolve, reject) => {
                    exec(`ping -c ${this.options.count} -W ${this.options.timeout} ${host.address}`, (err, stdout) => {
                        if (err) {
                            reject(err);
                            return;
                        }
                        const outputLines = stdout.split("\n");
                        let result = outputLines[outputLines.length - 2];

                        if (!result) {
                            reject(new Error("Not result"));
                            return;
                        }

                        result = result.match(/[\d]+(\.[\d]{1,})?/g)[1];

                        if (!result === undefined) {
                            reject(new Error("Not result"));
                            return;
                        }

                        resolve(new InputData(timestamp, { value: parseFloat(result) }, { host: host.name }));
                    });
                }));

            });

        return Promise.all(pings);
    }
}
