import Axios, {AxiosError, AxiosInstance} from "axios";
import IInput, {InputData, IValueList} from "./Input";
import {JsonConvert, JsonObject, JsonProperty} from "json2typescript";
import * as moment from "moment";
import {Moment} from "moment";
import {parseString} from "xml2js";

@JsonObject
class Lank3Options {
    /**
     * LANK3 host.
     * Default: `localhost`
     */
    @JsonProperty("host", String, true)
    host = "localhost";
    /**
     * LANK3 login.
     * Default: `''`
     */
    @JsonProperty("username", String, true)
    username = "";
    /**
     * LANK3 password.
     * Default: `|`
     */
    @JsonProperty("password", String, true)
    password = "";

    /**
     * LANK3 properties.
     * Default: `[]`
     */
    @JsonProperty("props", [String], true)
    props: string[] = [];
}

export default class Lank3 implements IInput {
    private options: Lank3Options;
    private axios: AxiosInstance;

    constructor(options = {}) {
        const convert = new JsonConvert();
        this.options = convert.deserialize(options, Lank3Options);
        this.axios = Axios.create();
    }

    getData(): Promise<InputData> {
        const timestamp: Moment = moment();
        return new Promise((resolve, reject) => {
            this.axios
                .get(`http://${this.options.host}/xml/ix.xml`, {
                    headers: {
                        "Content-Type": "text/plain;charset=UTF-8",
                        "Cookie": "admin:language=en",
                        "Pragma": "no-cache",
                    },
                    withCredentials: true,
                    auth: {
                        username: this.options.username,
                        password: this.options.password,
                    },
                })
                .then(response => {
                    try {
                        const tProps: IValueList = {};
                        parseString(response.data, (err, result) => {
                            if (err) {
                                reject(err);
                            }
                            Object.keys(result.response)
                                .forEach(key => {
                                    if (this.options.props.indexOf(key) > -1) {
                                        tProps[key] = parseFloat(result.response[key][0]);
                                    }
                                });
                        });
                        resolve(new InputData(timestamp, tProps));
                    } catch (e) {
                        reject(e);
                    }
                })
                .catch((err: AxiosError) => {
                    if (err.response) {
                        reject(new Error(`[${err.response.status}] ${err.response.statusText}`));
                    } else {
                        reject(new Error("Unexpected error"));
                    }
                });
        });
    }
}
