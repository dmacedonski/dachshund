import moment = require("moment");
import IInput, { InputData } from "./Input";

export default class RandomValueInput implements IInput {
    getData(): Promise<InputData> {
        return new Promise((resolve, reject) => {
            try {
                resolve(new InputData(moment(), { value: Math.floor(Math.random() * 100) }));
            } catch (e) {
                reject(e);
            }
        });
    }
}
