import { exec } from "child_process";
import moment = require("moment");
import IInput, { InputData } from "./Input";

export default class DiskUsageInput implements IInput {
    getData(): Promise<InputData[]> {
        return new Promise((resolve, reject) => {
            exec("df -l", (err, stdout) => {
                if (err) {
                    reject(err);
                } else {
                    const lines = stdout.split("\n");
                    lines.splice(0, 1);
                    const inputData: InputData[] = [];
                    const timestamp = moment();
                    for (let line of lines) {
                        line = line.replace(/\s\s+/g, " ");
                        const systemFile = line.split(" ");
                        if (systemFile[0].indexOf("/dev") >= 0) {
                            inputData.push(new InputData(
                                timestamp,
                                {
                                    usage: parseInt(systemFile[2], 10),
                                    total: parseInt(systemFile[2], 10) + parseInt(systemFile[3], 10),
                                    percent: parseInt(systemFile[4], 10),
                                },
                                {
                                    name: systemFile[0],
                                }),
                            );
                        }
                    }
                    resolve(inputData);
                }
            });
        });
    }
}
