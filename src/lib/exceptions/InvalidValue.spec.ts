import { expect } from "chai";
import { describe } from "mocha";
import InvalidValue from "./InvalidValue";

describe("InvalidValue", () => {
    it("InvalidValue should be instance of Error", () => {
        const invalidValue = new InvalidValue();
        expect(invalidValue).to.be.instanceof(Error);
    });

    describe("#constructor()", () => {
        it(",error message should to equal 'Invalid value'", () => {
            const invalidValue = new InvalidValue();
            expect(invalidValue.message).to.equal("Invalid value");
        });

        it("with message ,error message should to equal 'Invalid value: This is my message'", () => {
            const invalidValue = new InvalidValue("This is my message");
            expect(invalidValue.message).to.equal("Invalid value: This is my message");
        });

        it("with name ,error message should to equal 'Invalid value (foo)'", () => {
            const invalidValue = new InvalidValue(null, "foo");
            expect(invalidValue.message).to.equal("Invalid value (foo)");
        });

        it("with message and name ,error message should to equal 'Invalid value (foo): This is my message'", () => {
            const invalidValue = new InvalidValue("This is my message", "foo");
            expect(invalidValue.message).to.equal("Invalid value (foo): This is my message");
        });
    });

    describe("#notNull()", () => {
        it(",error message should to equal 'Invalid value: Must not be null'", () => {
            const invalidValue = InvalidValue.notNull();
            expect(invalidValue.message).to.equal("Invalid value: Must not be null");
        });

        it("with name ,error message should to equal 'Invalid value (foo): Must not be null'", () => {
            const invalidValue = InvalidValue.notNull("foo");
            expect(invalidValue.message).to.equal("Invalid value (foo): Must not be null");
        });
    });

    describe("#value()", () => {
        it(",error message should to equal 'Invalid value'", () => {
            const invalidValue = InvalidValue.value(2);
            expect(invalidValue.message).to.equal("Invalid value: 2");
        });

        it("with message ,error message should to equal 'Invalid value: This is my message: 2'", () => {
            const invalidValue = InvalidValue.value(2, "This is my message");
            expect(invalidValue.message).to.equal("Invalid value: This is my message: 2");
        });

        it("with name ,error message should to equal 'Invalid value (foo): 2'", () => {
            const invalidValue = InvalidValue.value(2, null, "foo");
            expect(invalidValue.message).to.equal("Invalid value (foo): 2");
        });

        it("with message and name ,error message should to equal 'Invalid value (foo): This is my message: 2'", () => {
            const invalidValue = InvalidValue.value(2, "This is my message", "foo");
            expect(invalidValue.message).to.equal("Invalid value (foo): This is my message: 2");
        });
    });
});
