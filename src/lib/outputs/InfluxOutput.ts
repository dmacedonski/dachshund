import {FieldType, InfluxDB, IPoint} from "influx";
import Application from "../Application";
import {InputData} from "../inputs/Input";
import InfluxConfigModel from "../models/InfluxConfigModel";
import IOutput from "./Output";

export default class InfluxOutput implements IOutput {
    protected options: InfluxConfigModel;
    private influx: InfluxDB;

    constructor(options = {}) {
        this.options = Application.config.influx.extend(options);
        this.influx = new InfluxDB({
            host: this.options.host,
            port: this.options.port,
            database: this.options.database,
            username: this.options.user,
            password: this.options.password,
            schema: [{
                measurement: this.options.schema.measurement,
                fields: this.options.schema.fields.reduce((preview, current) => {
                    preview[current.name] = current.type;
                    return preview;
                }, {} as { [index: string]: FieldType }),
                tags: this.options.schema.tags,
            }],
        });
    }

    sendData(inputData: InputData | InputData[]): Promise<void> {
        if (!Array.isArray(inputData)) {
            inputData = [inputData];
        }

        const points: IPoint[] = [];

        inputData.forEach(data => {
            points.push({
                measurement: this.options.schema.measurement,
                timestamp: data.timestamp,
                tags: data.tags,
                fields: data.values as { [index: string]: FieldType },
            });
        });

        try {
            return this.influx.writeMeasurement(this.options.schema.measurement, points, {
                precision: this.options.schema.precision,
                retentionPolicy: this.options.schema.retentionPolicy,
            });
        } catch (e) {
            return Promise.reject(e);
        }
    }
}
