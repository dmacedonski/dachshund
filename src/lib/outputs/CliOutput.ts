import { InputData } from "../inputs/Input";
import IOutput from "./Output";

export default class CliOutput implements IOutput {
    sendData(inputData: InputData | InputData[]): Promise<void> {
        return new Promise((resolve) => {
            console.log(inputData);
            resolve();
        });
    }
}
