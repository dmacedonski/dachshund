import { InputData } from "../inputs/Input";

export default interface IOutput {
    sendData(inputData: InputData | InputData[]): Promise<void>;
}
