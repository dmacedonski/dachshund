import { JsonConverter, JsonCustomConvert } from "json2typescript";
import InvalidValue from "../exceptions/InvalidValue";

export default class MACAddress {
    private readonly macReg = /^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/;
    private readonly mac: string;

    constructor(mac: string) {
        if (!this.macReg.test(mac)) {
            throw InvalidValue.value(mac, "is invalid MAC address");
        }
        this.mac = mac;
    }

    toString(): string {
        return this.mac;
    }
}

@JsonConverter
export class MACAddressConverter implements JsonCustomConvert<MACAddress> {
    serialize(data: MACAddress): string {
        return data.toString();
    }
    deserialize(data: any): MACAddress {
        if (typeof data !== "string") {
            throw new InvalidValue("value must be a string", "mac");
        }
        return new MACAddress(data);
    }
}
