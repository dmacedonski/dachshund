import { expect } from "chai";
import { before, describe } from "mocha";
import InvalidValue from "../exceptions/InvalidValue";
import Byte from "./Byte";

describe("Byte", () => {
    let byte: Byte = null;

    before(() => {
        byte = new Byte({
            bytes: 1,
            kibibytes: 1,
            mebibytes: 1,
            gibibytes: 1,
            tebibytes: 1,
        });
    });

    it("should return InvalidValue exception if size is less then 0", () => {
        const func = () => {
            return new Byte({ bytes: -1 });
        };
        expect(func).to.throw(InvalidValue);
    });

    it("bytes should to equal 1100586419201", () => {
        expect(byte.inBytes).to.equal(1100586419201);
    });

    it("kibibytes should to equal 1074791425", () => {
        expect(byte.inKibibytes).to.equal(1074791425);
    });

    it("mebibytes should to equal 1049601", () => {
        expect(byte.inMebibytes).to.equal(1049601);
    });

    it("gibibytes should to equal 1025", () => {
        expect(byte.inGibibytes).to.equal(1025);
    });

    it("tebibytes should to equal 1", () => {
        expect(byte.inTebibytes).to.equal(1);
    });
});
