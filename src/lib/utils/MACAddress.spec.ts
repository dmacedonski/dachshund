import { expect } from "chai";
import { describe } from "mocha";
import InvalidValue from "../exceptions/InvalidValue";
import MACAddress, { MACAddressConverter } from "./MACAddress";

describe("MACAddress", () => {
    it("should return InvalidValue exception if value is invalid MAC", () => {
        const func = () => {
            return new MACAddress("00:00:00");
        };

        expect(func).to.throw(InvalidValue);
    });

    describe("#toString", () => {
        it("should return the same value as given in constructor", () => {
            const macAsString = "00:00:00:00:00:00";
            const mac = new MACAddress(macAsString);
            expect(mac.toString()).to.equal(macAsString);
        });
    });
});

describe("MACAddressConverter", () => {
    describe("#deserialize", () => {
        let converter: MACAddressConverter = null;

        before(() => {
            converter = new MACAddressConverter();
        });

        it("should return InvalidValue exception if data is not string", () => {
            const func = () => {
                return converter.deserialize(3);
            };

            expect(func).to.throw(InvalidValue);
        });

        it("should return MACAddress object if data is valid MAC", () => {
            expect(converter.deserialize("00:00:00:00:00:00")).to.instanceof(MACAddress);
        });

        it("should return InvalidValue exception if data is invalid MAC", () => {
            const func = () => {
                return converter.deserialize("00:00:00");
            };

            expect(func).to.throw(InvalidValue);
        });
    });

    describe("#serialize", () => {
        let converter: MACAddressConverter = null;

        before(() => {
            converter = new MACAddressConverter();
        });

        it("should return 00:00:00:00:00:00 if data is MACAddress('00:00:00:00:00:00')", () => {
            expect(converter.serialize(new MACAddress("00:00:00:00:00:00"))).to.equal("00:00:00:00:00:00");
        });
    });
});
