import { expect } from "chai";
import { describe } from "mocha";
import InvalidValue from "../exceptions/InvalidValue";
import IPAddress, { IPAddressConverter } from "./IPAddress";

describe("IPAddress", () => {
    it("should return InvalidValue exception if value is incorrect IP", () => {
        const func = () => {
            return new IPAddress("0.0.0");
        };

        expect(func).to.throw(InvalidValue);
    });

    describe("#toString", () => {
        it("should return the same value as given in constructor", () => {
            const ipAsString = "0.0.0.0";
            const ip = new IPAddress(ipAsString);
            expect(ip.toString()).to.equal(ipAsString);
        });
    });
});

describe("IPAddressConverter", () => {
    describe("#deserialize", () => {
        let converter: IPAddressConverter = null;

        before(() => {
            converter = new IPAddressConverter();
        });

        it("should return InvalidValue exception if data is not string", () => {
            const func = () => {
                return converter.deserialize(3);
            };

            expect(func).to.throw(InvalidValue);
        });

        it("should return IPAddress object if data is valid IP", () => {
            expect(converter.deserialize("127.0.0.1")).to.instanceof(IPAddress);
        });

        it("should return InvalidValue exception if data is not valid IP", () => {
            const func = () => {
                return converter.deserialize("127.0.0");
            };

            expect(func).to.throw(InvalidValue);
        });
    });

    describe("#serialize", () => {
        let converter: IPAddressConverter = null;

        before(() => {
            converter = new IPAddressConverter();
        });

        it("should return 127.0.0.1 if data is IPAddress('127.0.0.1')", () => {
            expect(converter.serialize(new IPAddress("127.0.0.1"))).to.equal("127.0.0.1");
        });
    });
});
