import { JsonConverter, JsonCustomConvert } from "json2typescript";
import InvalidValue from "../exceptions/InvalidValue";

export default class IPAddress {
    private readonly ipReg = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
    private readonly ip: string;

    constructor(ip: string) {
        if (!this.ipReg.test(ip)) {
            throw InvalidValue.value(ip, "is not correct IP address");
        }
        this.ip = ip;
    }

    toString(): string {
        return this.ip;
    }
}

@JsonConverter
export class IPAddressConverter implements JsonCustomConvert<IPAddress> {
    serialize(data: IPAddress): string {
        return data.toString();
    }
    deserialize(data: any): IPAddress {
        if (typeof data !== "string") {
            throw new InvalidValue("value must be a string", "ip");
        }
        return new IPAddress(data);
    }
}
