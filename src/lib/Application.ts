import * as fs from "fs";
import { JsonConvert } from "json2typescript";
import * as moment from "moment";
import * as path from "path";
import * as winston from "winston";
import * as yaml from "yaml";
import ConfigModel from "./models/ConfigModel";
import Register from "./Register";
import Task from "./Task";
import Byte from "./utils/Byte";

export default class Application {
    static config: ConfigModel;
    private readonly logger: winston.Logger;
    private readonly register: Register;
    private readonly tasks: Task[] = [];

    constructor() {
        process.title = "dachshund-service";
        this.logger = winston.createLogger({
            level: "info",
            transports: [
                new winston.transports.Console({
                    format: winston.format.combine(
                        winston.format.timestamp({ format: this.timestampFormat.bind(this) }),
                        winston.format.colorize(),
                        winston.format.printf(info => {
                            return `${info.timestamp} ${info.level}: ${info.message}`;
                        }),
                    ),
                }),
                new winston.transports.File({
                    dirname: path.resolve(__dirname, "..", "..", "log"),
                    filename: "dachshund.log",
                    maxFiles: 10,
                    maxsize: new Byte({ mebibytes: 10 }).inBytes,
                    format: winston.format.combine(
                        winston.format.timestamp({ format: this.timestampFormat.bind(this) }),
                        winston.format.printf(info => {
                            return `${info.timestamp} [${info.level.toUpperCase()}] ${info.message}`;
                        }),
                    ),
                }),
            ],
        });
        this.register = new Register();
    }

    run(): void {
        try {
            this.loadConfiguration();
            this.logger.level = Application.config.logLevel;
            this.runTask();
            this.logger.info("Dachshund Service started successfully.");
        } catch (e) {
            this.logger.error(e.message);
            process.exit(1);
        }
    }

    private timestampFormat(): string {
        return moment().utc().format("YYYY-MM-DD HH:mm:ss.SSS");
    }

    private loadConfiguration(): void {
        let config: string;
        const convert = new JsonConvert();

        if (process.argv[2]) {
            if (!fs.existsSync(process.argv[2])) {
                throw new Error(`Config file ${process.argv[2]} not exists.`);
            }
            config = fs.readFileSync(process.argv[2]).toString();
        } else {
            const defaultConfigPath = path.resolve(__dirname, "..", "..", "config", "default.yml");
            if (!fs.existsSync(defaultConfigPath)) {
                throw new Error(`Default config file ${defaultConfigPath} not exists.`);
            }
            config = fs.readFileSync(defaultConfigPath).toString();
        }

        Application.config = convert.deserialize(yaml.parse(config), ConfigModel);
    }

    private runTask(): void {
        Application.config.tasks
            .forEach(task => {
                this.tasks.push(new Task(
                    task.interval,
                    task.name,
                    this.register.getInput(task.input.name, task.input.options, this.logger),
                    this.register.getOutput(task.output.name, task.output.options),
                    this.logger,
                ));
            });
        this.tasks
            .forEach(task => {
                task.run();
            });
    }
}
