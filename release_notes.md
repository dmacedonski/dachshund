# Release notes

## v0.6.0

- Add lank3 (LAN KONTROLER V3) input plugin.
- Add hddtemp input plugin.
- Fix auth bug for influx plugin.
- Update packages dependence.
- Update documentation.

## v0.5.0

- Add sensors input plugin

## v0.4.0

- Add speedtest input plugin.
- Update documentation.

## v0.3.1

- Fix types in traffic statistics.

## v0.3.0

- Add network traffic statistics plugin `traffic_stats_input` for router Tenda with software 5.x version.
- Update documentation.

## v0.2.0

- Add `voltage_input` plugins.
- Add [documentation](doc/index.md).
- Change load plugins method.
