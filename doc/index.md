# Documentation

## Table of Contents

1. [Configuration file](configuration.md)
2. List of plugins
   1. [Input plugins](input-plugins/index.md)
   2. [Output plugins](output-plugins/index.md)