# LANK3 plugin

Returns information from sensors connected to [Lancontroller3](https://tinycontrol.pl/pl/dokumentacja-i-firmware/) device. This plugin uses  LANK3 http API.

## How to use

Input response:

```
InputData {
  timestamp: 1541845195300,
  values:
   { out5: 0,
     pwm: 1,
     vin: 1215,
     tem: 3800,
     ds2: 258,
     ds3: 291,
     power1: 1224489 },
  tags: {} }
```

Example configuration:

```yaml
tasks:
- name: LANK3
  interval: 30
  input:
    name: lank3
    options:
      host: 192.168.1.131
      username: USER		 #set if auth is enabled
      password: PASSWORD	 #set if auth is enabled
      props:
      - vin
      - tem
      - ds2
      - ds3
      - power1
      - pwm
      - out5
  output:
    name: influx
    options:
      database: system
      schema:
        measurement: lank3
        fields:
        - name: vin
          type: float
        - name: tem
          type: float
        - name: ds2
          type: float
        - name: ds3
          type: float
        - name: power1
          type: float
        - name: pwm
          type: float
        - name: out5
          type: float
```

## Plugins return object structure

| Name   | Type     | Description      |
| ------ | -------- | ---------------- |
| `vin` | `number` | Input Voltage [cV]. |
| `tem` | `number` | Board temperature [c°C] |
| `ds1-ds8` | `number` | Sensor value |
| `out0-out5` | `number` | Output status `[0,1]` |
| `pwm` | `number` | PWM status `[0,1]` |
