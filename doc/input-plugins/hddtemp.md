# HDDTEMP plugin

Return information about disk temperature. This plugin uses `hddtemp` daemon. It reads daemon's socket, default `localhost:7634` with delimiter `|`.

## How to use

Install `hddtemp` package:

```bash
apt-get install hddtemp
nano /etc/default/hddtemp
systemctl restart hddtemp.service
```

Input response:

```
[ 
InputData {
    timestamp: 1541844203752,
    values: { temp: 44 },
    tags: { device: '/dev/sda', name: 'ADATA SU800' } 
    },
InputData {
    timestamp: 1541844203752,
    values: { temp: 39 },
    tags: { device: '/dev/sdb', name: 'WDC WD30EFRX-68N32N0' } 
    } 
]
```

Example configuration:

```yaml
tasks:
- name: HddTemp
  interval: 10
  input:
    name: hddtemp
    options:
      host: localhost   #default value
      port: 7634        #default value
      delimiter: "|"    #default value, `|` is special yaml char, escape it
  output:
    name: influx
    options:
      database: system
      schema:
        measurement: hddtemp
        fields:
        - name: temp
          type: float
        tags:
        - name
        - device
```

## Plugins return object structure

| Name   | Type     | Description      |
| ------ | -------- | ---------------- |
| `temp` | `number` | Temperature in V. |
