# Traffic statistics input plugins

Return information network traffic. The data are taken from the external devices, eg. Router. List of supported devices below.

## List of supported devices

Use below names as `model` in configuration.

- `router_tenda_v5` - [Tenda](http://tendacn.com) with software version 5.x

## How to use

Set `traffic_stats_input` as input name.

```yaml
tasks:
	- name: traffic_stats_example
	  input: traffic_stats_input
	  input_options:
	  	model: router_tenda_v5
	  	host: '192.168.0.1'
	  output: stdout_output
```

## Plugins options

| Option name | Type     | Default | Description                   |
| ----------- | -------- | ------- | ----------------------------- |
| `model`     | `string` | `null`  | Device name.                  |
| `host`      | `string` | `null`  | Device ip address.            |
| `user`      | `string` | `null`  | [optional] User name          |
| `password`  | `string` | `null`  | [optional] Password to device |

## Plugins return object structure

Plugin return array of below object.

| Name            | Type     | Description                                    |
| --------------- | -------- | ---------------------------------------------- |
| `ipAddress`     | `string` | Host ip address.                               |
| `mac`           | `string` | [optional] Host mac address.                   |
| `name`          | `string` | Host name, if not exists is mac or ip address. |
| `upload`        | `number` | Upload current speed in bytes.                 |
| `download`      | `number` | Download current speed in bytes.               |
| `sentBytes`     | `number` | Sent data in bytes.                            |
| `receivedBytes` | `number` | Received dara in bytes.                        |

