# Disk usage input plugins

Return infomration about disk usage.

## How to use

Set `disk_usage_input` as input name.

```yaml
tasks:
	- name: disk_usage_example
	  input: disk_usage_input
	  output: stdout_output
```

## Plugins return object structure

Plugin return array of below object.

| Name      | Type     | Description                  |
| --------- | -------- | ---------------------------- |
| `name`    | `string` | Disk name like: `/dev/sda1`. |
| `usage`   | `number` | Number of usage bytes.       |
| `total`   | `number` | Number of total bytes.       |
| `percent` | `number` | Number of usage in percents. |