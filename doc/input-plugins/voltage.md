# Voltage input plugins

Return infomration about Raspberry Pi voltage. This plugins use `/opt/vc/bin/vcgencmd` commad to get voltage value.

## How to use

Set `voltage_input` as input name.

```yaml
tasks:
	- name: voltage_example
	  input: disk_usage_input
	  output: stdout_output
```

## Plugins return object structure

| Name      | Type     | Description   |
| --------- | -------- | ------------- |
| `voltage` | `number` | Voltage in V. |