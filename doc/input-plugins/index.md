# List of input plugins

Any plugins return object, you can read more about plugins click on one of the following.

1. [Random](random.md) - Return random number
2. [Disk Usage](disk_usage.md) - Return disk usage stats
3. [Temperature](temperature.md) - Return current Raspberry Pi temperature
4. [Voltage](voltage.md) - Return current Raspbery Pi voltage
5. [Traffic statistics](traffic-stats.md) - Return network traffic statistics
6. [Speed test](speedtest.md) - Return data from Speedtest

