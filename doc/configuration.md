# Configuration

## Main configuration

| Name        | Type                                                         | Default | Description                                                |
| ----------- | ------------------------------------------------------------ | ------- | ---------------------------------------------------------- |
| `log_level` | enum:  `'error'` `'warn'` `'info'` `'verbose'` `'debug'` `'silly'` | `info`  | Log level, logs save to the file `logs/dachshund.log`. |
| `tasks`     | Array of `task`                                              | `[]`    | List of tasks to run.                                      |

## Task structure

| Name             | Type                 | Default | Description                                                  |
| ---------------- | -------------------- | ------- | ------------------------------------------------------------ |
| `name`           | `string`             | `null`  | Unique task name.                                            |
| `interval`       | `number` or `object` | `null`  | [optional] Number of seconds or object with time. If not set task run only one time. |
| `input`          | `string`             | `null`  | Input plugin name. List [here](input-plugins/index.md).      |
| `input_options`  | `object`             | `null`  | [optional] Input plugin options.                             |
| `output`         | `string`             | `null`  | Output plugin name. List [here](output-plugins/index.md).    |
| `output_options` | `string` or `object` | `null`  | [optional] Output plugin options.                            |

## Interval object structure

| Name           | Type     | Default | Description                         |
| -------------- | -------- | ------- | ----------------------------------- |
| `hours`        | `number` | `0`     | [optional] Numbers of hours.        |
| `minutes`      | `number` | `0`     | [optional] Numbers of minutes.      |
| `seconds`      | `number` | `0`     | [optional] Numbers of seconds.      |
| `milliseconds` | `number` | `0`     | [optional] Numbers of milliseconds. |

## Configuration example

```yaml
log_level: debug
tasks:
  - name: my_first_job
    interval:
      minutes: 1
    input: random_input
    input_options:
      minValue: 1
      maxValue: 100
    output: stdout_output
  - name: my_second_job
    interval:
      hours: 1
      minutes: 30
    input: temperature_input
    output: fluent_output
    output_options:
      tag: temperature
      tagPrefix: system
```

