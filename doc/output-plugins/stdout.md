# Stdout output plugins

Display data in console, usefull to debug.

## How to use

Use name `stdout_output` in configuration as output.

```yaml
tasks:
	- name: stdout_example
	  input: random_input
	  output: stdout_output
```

